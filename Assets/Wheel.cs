﻿using UnityEngine;
using System.Collections;

public class Wheel : MonoBehaviour {
	
	private float mfDegAngleX = 0;
	private float mfDegAngleY = 0;
	private float mfPerimeter = 0;
	private float mfDeltaDistance;
	
	public bool mbIsFront = false;
	
	public float DegAngleY {
		get {
			return this.mfDegAngleY;
		}
		set {
			mfDegAngleY = value;
		}
	}
	
	public float DeltaDistance {
		get {
			return this.mfDeltaDistance;
		}
		set {
			mfDeltaDistance = value;
		}
	}

	void Start () {
		// Get wheels perimeter
        MeshFilter meshFilter = this.transform.GetChild(0).GetComponent<MeshFilter>();
        if (meshFilter != null)
        {
            Mesh mesh = meshFilter.mesh;
            if (mesh != null)
            {
                mfPerimeter = mesh.bounds.size.z * Mathf.PI * this.transform.GetChild(0).localScale.z;
            }
        }
	}

	void Update () {
		mfDegAngleX += mfDeltaDistance/mfPerimeter*360;
		mfDegAngleX %= 360;
		this.transform.rotation = this.transform.parent.rotation*Quaternion.Euler(mfDegAngleX, mbIsFront ? mfDegAngleY : 0, 0);
	}
}
