﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Car : MonoBehaviour {
	
	private Vector3 mvLastPos;
	private float mfSpeed = 0;
	private float mfDirection = 0;
	
	
	public float mfAcceleration = 0.02f;
	public float mfDeceleration = 0.02f;
	public float mfBrakeDeceleration = 0.05f;
	public float mfWheelMaxDegY = 45;
    public float mfMaxSpeed = 1;
    public List<GameObject> mgWheels = new List<GameObject>();
	public List<GameObject> mgBackLights = new List<GameObject>();
	
	public float Speed {
		get {
			return this.mfSpeed;
		}
		set {
			mfSpeed = value;
		}
	}

	void Start () {
		mvLastPos = this.transform.position;
	}
	
	void Update () {
		UpdateCar();
		
        if (Input.GetAxis("Vertical") != 0) {
			Accelerate();
        } else {
			Decelerate();
        }
		if (Input.GetKey(KeyCode.Space)) {
			Brake();
			SetLightIntensity(0.4f);
		} else {
			SetLightIntensity(0.2f);
		}
		
		EndFrame();
    }
	
	void Accelerate()
	{
		mfSpeed = Mathf.Clamp(mfSpeed + mfAcceleration, 0, mfMaxSpeed);
	}
	
	void Decelerate()
	{
		mfSpeed = Mathf.Clamp(mfSpeed - mfDeceleration, 0, mfMaxSpeed);
	}
	
	void Brake()
	{
		mfSpeed = Mathf.Clamp(mfSpeed - mfBrakeDeceleration, 0, mfMaxSpeed);
	}
	
	void UpdateCar()
	{
		// Update Direction
		if (Input.GetAxis("Vertical") != 0) {
			mfDirection = Mathf.Sign(Input.GetAxis("Vertical"));
		}
		if (mfSpeed == 0) {
			mfDirection = 0;
		}
		// Update Rotation
		this.transform.Rotate(0, mfSpeed * 1.5f * Input.GetAxis("Horizontal") * mfDirection, 0);
		
		// Update Position
		this.transform.position += mfDirection * this.transform.forward * mfSpeed;
		
		// Update Wheels (XRotation, YRotation)
		Vector3 tvDeltaPos = this.transform.position - mvLastPos;
		float tfDistance = tvDeltaPos.magnitude;
		foreach (GameObject tgWheel in mgWheels)
        {
            tgWheel.GetComponent<Wheel>().DeltaDistance = mfDirection * tfDistance;
			tgWheel.GetComponent<Wheel>().DegAngleY = Input.GetAxis("Horizontal") * mfWheelMaxDegY;
        }
	}
	
	void SetLightIntensity(float intensity)
	{
		foreach (GameObject tgBackLight in mgBackLights)
        {
            tgBackLight.light.intensity = intensity;
        }
	}
	
	void EndFrame()
	{
		mvLastPos = this.transform.position;	
	}
	
}
